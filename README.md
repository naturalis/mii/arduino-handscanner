# arduino-handscanner

Interactive: de Dood / Niets gaat verloren

De bezoeker kan een filmpje starten door een hand op een daarvoor aagegeven plek te leggen.

Werking script:

In setup() wordt een timer geactiveerd die 100 keer per seconde een interrupt genereerd.
In deze interrupt wordt de toestand van de sensor gemeten. Als in een aantal opeenvolgende interrupts de zelfde toestand wordt gemeten
dan wordt dit middels een flag doorgegeven aan de mainloop. Deze methode voorkomt dat eventuele spikes uit de sensor voor ongewenst
starten van de film veroorzaakt. Met MaxSensIntr kan de snelheid van om-schakelen worden ingesteld.

In het loop() gedeelte van het script wordt gekeken of de interrupt flag actief is. Als dit zo is wordt de nieuwe toestand van de sensor
als keyboard events aan de host (NUC) doorgegeven.

Aansluiting sensor op Arduino:
De sensor wordt gevoed door een externe 5 volt voeding. De sensor wordt dmv een opto coupler galvanisch van de arduino gescheiden.
Op de sensorprint wordt het signaal van de optocoupler naar buiten gevoerd via de E (emitter) en C (collector) aansluitingen.
Er wordt aangesloten op de Arduino Gnd  en C op input 1. (Dat is pin A0). A0 moet in setup als input met pull-up weerstand gedefineerd worden.
